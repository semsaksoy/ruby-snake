# frozen_string_literal: true

require 'gosu'

class Oyun < Gosu::Window
  def initialize
    super(1280, 1000)
    self.caption = 'tencere'
    @polis = Gosu::Image.new(self, 'assets/police.png', false, 0, 0)
    @ates = Gosu::Image.new(self, 'assets/fire.png', false, 0, 0)
    @apple = Gosu::Image.new(self, 'assets/apple.png', false, 0, 0)
    @apple_font = Gosu::Font.new(self, 'Comic Sans', 60)
    @hamm = Gosu::Sample.new('assets/hamm.ogg')
    @vissh = Gosu::Sample.new('assets/vissh2.ogg')
    @motor = Gosu::Sample.new('assets/motor.ogg')
    @motor_channel = nil
    @vissh_playing = false
    @polis_x = 100
    @polis_y = 100
    @polis_a = 0
    @turn_side = 'none'
    @go_forward = false
    @firing = false
    @police_v = 6
    @apple_x = rand(width)
    @apple_y = rand(height)
    @apple_n = 0
    @eating = false
  end

  def update
    # kontrol et
    if @polis_x > width
      @polis_x = 0
    elsif @polis_x.negative?
      @polis_x = width
    end
    if @polis_y > height
      @polis_y = 0
    elsif @polis_y.negative?
      @polis_y = height
    end

    @eating = (@polis_x < @apple_x + 50) &&
              (@polis_x > @apple_x - 50) &&
              (@polis_y < @apple_y + 50) &&
              (@polis_y > @apple_y - 50)

    @turn_side = if Gosu.button_down? Gosu::KB_LEFT
                   'left'
                 elsif Gosu.button_down? Gosu::KB_RIGHT
                   'right'
                 else
                   'none'
                 end
    @go_forward = Gosu.button_down? Gosu::KB_UP
    if @go_forward
      if @motor_channel.nil? || !@motor_channel.playing?
        @motor_channel = @motor.play(1, 1, true)
      end    
    end
    @firing = Gosu.button_down? Gosu::KB_RIGHT_ALT
    @police_v = if @firing
                  @vissh.play unless @vissh_playing
                  @vissh_playing = true
                  18
                else
                  @vissh_playing = false
                  6
                end

    # islet
    if @turn_side == 'left'
      @polis_a -= 5
    elsif @turn_side == 'right'
      @polis_a += 5
    end
    if @eating
      @apple_x = rand(width)
      @apple_y = rand(height)
      @apple_n += 1
      @eating = false
      @hamm.play
    end
    return unless @go_forward

    @polis_x += @police_v * Math.cos(@polis_a * Math::PI / 180)
    @polis_y += @police_v * Math.sin(@polis_a * Math::PI / 180)
  end

  def draw
    @polis.draw_rot(@polis_x, @polis_y, 1, @polis_a, 0.5, 0.5, 0.2, 0.2)
    @apple.draw_rot(@apple_x, @apple_y, 0, 0, 0.5, 0.5, 0.2, 0.2)
    @apple_font.draw(@apple_n.to_s, 50, 50, 0)
    return unless @firing

    @ates.draw_rot(@polis_x, @polis_y, 1, @polis_a + 180, -1, 0.5, 0.1, 0.1)
  end
end

oyun = Oyun.new
oyun.show
